const chai = require('chai');
const chaiHttp = require('chai-http');
const app = require('../app');
chai.use(chaiHttp);
chai.should();
describe('Show the get result', function () {
    it('Displays hello world', function (done) {
        chai.request(app)
            .get('/')
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.have.property('message');
                done();
            });
    });
});
